﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { PatientData, AppointmentData, UserSimpleData } from './FetchPatient';  

interface AddAppointmentDataState {
    title: string;
    loading: boolean;
    doctorList: UserSimpleData[];
    patientList: PatientData[];
    empData: AppointmentData;
}
export class AddAppointment extends React.Component<RouteComponentProps<{}>, AddAppointmentDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "", loading: true, doctorList: [], patientList: [], empData: new AppointmentData };
        fetch('api/Doctors')
            .then(response => response.json() as Promise<UserSimpleData[]>)
            .then(data => {
                this.setState({ doctorList: data });
            });
        fetch('api/Patients')
            .then(response => response.json() as Promise<PatientData[]>)
            .then(data => {
                this.setState({ patientList: data });
            });

        var empid = this.props.match.params["empid"];
        // This will set state for Edit employee  
        if (empid > 0) {
            fetch('api/Appointments/Details/' + empid)
                .then(response => response.json() as Promise<AppointmentData>)
                .then(data => {
                    this.setState({ title: "Edit", loading: false, empData: data });
                });
        }
        // This will set state for Add employee  
        else {
            this.state = { title: "Create", loading: false, doctorList: [], patientList: [], empData: new AppointmentData };
        }
        // This binding is necessary to make "this" work in the callback  
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm(this.state.doctorList, this.state.patientList);
        return <div>
            <h1>{this.state.title}</h1>
            <h3>Employee</h3>
            <hr />
            {contents}
        </div>;
    }
    // This will handle the submit form event.  
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        var doc = document.getElementById('selectDoctor');
        var pat = document.getElementById('selectPatient');
        
        // PUT request for Edit employee.  
        if (this.state.empData.id) {
            fetch('api/Appointments/Edit', {
                method: 'PUT',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchappointments");
                })
        }
        // POST request for Add employee.  
        else {
            fetch('api/Appointments', {
                method: 'POST',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchappointments");
                })
        }
    }
    // This will handle Cancel button click event.  
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/fetchappointments");
    }
    // Returns the HTML Form to the render() method.  
    private renderCreateForm(doctorList: UserSimpleData[], patientList: PatientData[]) {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="employeeId" value={this.state.empData.id} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Time">Time</label>
                    <div className="col-md-4">
                        <input className="form-control" type="datetime-local" name="time" defaultValue={this.state.empData.time} required />
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Cost" >Cost</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Cost"  required />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Doctor">Doctor</label>
                    <div className="col-md-4">
                        <select id = "selectDoctor" className="form-control" data-val="true" name="DoctorId"  required>
                            <option value="">-- Select Doctor --</option>
                            {doctorList.map(doctor =>
                                <option key={doctor.id} value={doctor.id}> {doctor.firstName} {doctor.lastName} </option>
                        )}
                        </select>
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Patient">Patient</label>
                    <div className="col-md-4">
                        <select id = "selectPatient" className="form-control" data-val="true" name="PatientId"  required>
                            <option value="">-- Select Patient --</option>
                            {patientList.map(patient =>
                                <option key={patient.id} value={patient.id}>  {patient.firstName} {patient.lastName} </option>
                            )}
                        </select>
                    </div>
                </div >

                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}