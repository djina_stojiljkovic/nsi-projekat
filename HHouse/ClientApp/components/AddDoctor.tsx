﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { DoctorData } from './FetchDoctors';
interface AddDoctorDataState {
    title: string;
    loading: boolean;

    empData: DoctorData;
}
export class AddDoctor extends React.Component<RouteComponentProps<{}>, AddDoctorDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "", loading: true, empData: new DoctorData };

        var empid = this.props.match.params["empid"];
        // This will set state for Edit employee  
        if (empid > 0) {
            fetch('api/Doctors/Details/' + empid)
                .then(response => response.json() as Promise<DoctorData>)
                .then(data => {
                    this.setState({ title: "Edit", loading: false, empData: data });
                });
        }
        // This will set state for Add employee  
        else {
            this.state = { title: "Create", loading: false, empData: new DoctorData };
        }
        // This binding is necessary to make "this" work in the callback  
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm();
        return <div>
            <h1>{this.state.title}</h1>
            <h3>Edit Doctor</h3>
            <hr />
            {contents}
        </div>;
    }
    // This will handle the submit form event.  
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        // PUT request for Edit employee.  
        if (this.state.empData.id) {
            fetch('api/Doctors/Edit/' + this.state.empData.id, {
                method: 'PUT',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchdoctors");
                })
        }
        // POST request for Add employee.  
        else {
            fetch('api/Doctors', {
                method: 'POST',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchdoctors");
                })
        }
    }
    // This will handle Cancel button click event.  
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/fetchdoctors");
    }
    // Returns the HTML Form to the render() method.  
    private renderCreateForm() {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="Id" value={this.state.empData.id} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="First Name">First Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="firstname" defaultValue={this.state.empData.firstName} required />
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Last Name">Last Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="lastname" defaultValue={this.state.empData.lastName} required />
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Specialty">Specialty</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="specialty" defaultValue={this.state.empData.specialty} required />
                    </div>
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="OrdinationNumber">Office</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="ordinationNumber" defaultValue={this.state.empData.ordinationNumber.toString()} required />
                    </div>
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="PhoneNumber">Phone</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="phoneNumber" defaultValue={this.state.empData.phoneNumber} required />
                    </div>
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form>)
        }
    }
               
            