﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink, } from 'react-router-dom';
interface FetchDoctorDataState {
    doctorList: DoctorData[];
    loading: boolean;
}
export class FetchDoctors extends React.Component<RouteComponentProps<{}>, FetchDoctorDataState> {
    constructor() {
        super();
        this.state = { doctorList: [], loading: true };
        fetch('api/Doctors')
            .then(response => response.json() as Promise<DoctorData[]>)
            .then(data => {
                this.setState({ doctorList: data, loading: false });
            });
        // This binding is necessary to make "this" work in the callback  
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderPatientTable(this.state.doctorList);
        return <div>
            <h1>Doctors</h1>
            <p>
                <Link to="/adddoctor">Add New Doctor</Link>
            </p>
            {contents}
        </div>;
    }
    // Handle Delete request for an employee  
    private handleDelete(id: number) {
        if (!confirm("Do you want to delete doctor with Id: " + id))
            return;
        else {
            fetch('api/Doctors/' + id, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        doctorList: this.state.doctorList.filter((rec) => {
                            return (rec.id != id);
                        })
                    });
            });
        }
    }
    private handleEdit(id: number) {
        this.props.history.push("/doctor/edit/" + id);
    }
    private handleView(id: number) {
        this.props.history.push("/doctor/details/" + id);
    }
    // Returns the HTML table to the render() method.  
    private renderPatientTable(doctorList: DoctorData[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Specialty</th>
                    <th>Office</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody>
                {doctorList.map(emp =>
                    <tr key={emp.id}>
                        <td></td>
                        <td>{emp.firstName}</td>
                        <td>{emp.lastName}</td>
                        <td>{emp.specialty}</td>
                        <td>{emp.ordinationNumber}</td>
                        <td>{emp.phoneNumber}</td>
                        <td>
                            <a className="action" onClick={(id) => this.handleEdit(emp.id)}>Edit</a>  |
                            <a className="action" onClick={(id) => this.handleDelete(emp.id)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

export class DoctorData {
    id: number = 0;
    firstName: string = "";
    lastName: string = "";
    specialty: string = " ";
    ordinationNumber: number = 0;
    phoneNumber: string = "";
}