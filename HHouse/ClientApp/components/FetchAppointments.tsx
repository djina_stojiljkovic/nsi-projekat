﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX

import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { UserSimpleData } from './FetchPatient';
import { Therapy } from './FetchPatient';
import { AppointmentData, PatientData } from './FetchPatient';


interface FetchAppointmentDataState {
    appointmentList: AppointmentData[];
    patientList: string[];
    loading: boolean;
}
export class FetchAppointments extends React.Component<RouteComponentProps<{}>, FetchAppointmentDataState> {
    constructor() {
        super();
        this.state = { appointmentList: [], loading: true, patientList: [] };
        fetch('api/Appointments')
            .then(response => response.json() as Promise<AppointmentData[]>)
            .then(data => {
                this.setState({ appointmentList: data, loading: false });
                for (var i = 0; i < this.state.appointmentList.length; i++) {
                    this.getPatient(this.state.appointmentList[i].patientId);
                }
            });
        // This binding is necessary to make "this" work in the callback  
        this.handleDelete = this.handleDelete.bind(this);

    }
    
    // Handle Delete request for an employee  
    private handleDelete(id: number) {
        if (!confirm("Do you want to delete appointment with Id: " + id))
            return;
        else {
            fetch('api/Appointments/' + id, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        appointmentList: this.state.appointmentList.filter((rec) => {
                            return (rec.id != id);
                        })
                    });
            });
        }
    }

    private getPatient(id: number) {
        fetch('api/Patients/Name/' + id)
            .then(response => response.json() as Promise<string>)
            .then(data => {
                this.state.patientList.push(data);
            });
    }

    private handleView(id: number) {
        this.props.history.push("/appointment/details/" + id);
    }
    // Returns the HTML table to the render() method.  
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderAppointmentTable(this.state.appointmentList);
        return <div>
            <h1>Upcoming Appointments</h1>
            <p>
                <Link to="/addappointment">Add New Appointment</Link>
            </p>
            {contents}
        </div>;
    }
    private renderAppointmentTable(appointmentList: AppointmentData[]) {
        return (<table className='table'>
            <thead>
                <tr>
                    <th>Cost</th>
                    <th>Diagnosis</th>
                    <th>Time</th>
                    <th>Doctor</th>
                    <th>Field</th>
                </tr>
            </thead>
            <tbody>
                {appointmentList.map((emp) =>
                    <tr key={emp.id}>
                        <td>{emp.cost}</td>
                        <td>{emp.diagnosis}</td>
                        <td>{emp.time}</td>
                        <td>{emp.doctor.firstName} {emp.doctor.lastName}</td>
                        <td>{emp.doctor.specialty}</td>


                        <td>
                            <a className="action" onClick={(id) => this.handleView(emp.id)}>View</a>  |
                            <a className="action" onClick={(id) => this.handleDelete(emp.id)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>
        )

    }
}

