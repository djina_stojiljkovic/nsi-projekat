﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { StringData, AppointmentData } from './FetchPatient';

interface AddDiagnosisDataState {
    title: string;
    loading: boolean;
    empData: StringData;
    //diagnosis: String;
}
export class AddDiagnosis extends React.Component<RouteComponentProps<{}>, AddDiagnosisDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "", loading: true, empData: new StringData };

        var empid = this.props.match.params['empid'];

        this.state = { title: "Create", loading: false, empData: new StringData};

        
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm();
        return <div>
            <h1>{this.state.title}</h1>
           
            <hr />
            {contents}
        </div>;
    }
    // This will handle the submit form event.  
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        // PUT request for Edit employee.  
        
        // POST request for Add employee.  
        
        fetch('api/Appointments/AddDiagnosis/' + this.props.match.params['empid'], 
            {
                method: 'POST',
                body: data
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/appointment/details/" + this.props.match.params['empid']);
                })
       // this.props.history.push("/appointment/details/" + this.props.match.params['empid']);
    }
    // This will handle Cancel button click event.  
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/appointment/details/" + this.props.match.params['empid']);
    }
    // Returns the HTML Form to the render() method.  
    private renderCreateForm() {
        return (
            <form onSubmit={this.handleSave} >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Diagnosis">Diagnosis</label>
                    <div className="col-md-4">
                        <input id="diag" className="form-control" type="text" name="diagnosis" required />
                    </div>
                </div >
                

                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}