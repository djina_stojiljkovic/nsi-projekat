﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
interface FetchPatientDataState {
    patientList: PatientData[];
    loading: boolean;
}
export class FetchPatient extends React.Component<RouteComponentProps<{}>, FetchPatientDataState> {
    constructor() {
        super();
        this.state = { patientList: [], loading: true };
        fetch('api/Patients')
            .then(response => response.json() as Promise<PatientData[]>)
            .then(data => {
                this.setState({ patientList: data, loading: false });
            });
        // This binding is necessary to make "this" work in the callback  
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderPatientTable(this.state.patientList);
        return <div>
            <h1>Patients</h1>
            <p>
                <Link to="/addpatient">Add New Patient</Link>
            </p>
            {contents}
        </div>;
    }
    // Handle Delete request for an employee  
    private handleDelete(id: number) {
        if (!confirm("Do you want to delete patient with Id: " + id))
            return;
        else {
            fetch('api/Patients/' + id, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        patientList: this.state.patientList.filter((rec) => {
                            return (rec.id != id);
                        })
                    });
            });
        }
    }
    private handleEdit(id: number) {
        this.props.history.push("/patient/edit/" + id);
    }
    private handleView(id: number) {
        this.props.history.push("/patient/details/" + id);
    }
    // Returns the HTML table to the render() method.  
    private renderPatientTable(patientList: PatientData[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Id Number</th>
                    <th>DoB</th>
                </tr>
            </thead>
            <tbody>
                {patientList.map(emp =>
                    <tr key={emp.id}>
                        <td></td>
                        <td>{emp.firstName}</td>
                        <td>{emp.lastName}</td>
                        <td>{emp.jmbg}</td>
                        <td>{emp.dateOfBirth}</td>
                        <td>
                            <a className="action" onClick={(id) => this.handleView(emp.id)}>View</a>  |
                            <a className="action" onClick={(id) => this.handleEdit(emp.id)}>Edit</a>  |
                            <a className="action" onClick={(id) => this.handleDelete(emp.id)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}
export class PatientData {
    id: number = 0;
    firstName: string = "";
    lastName: string = "";
    jmbg: string = "";
    note: string = "";
    dateOfBirth: string = "";
    appointments: AppointmentData[] = [];
}

export class AppointmentData {
    id: number = 0;
    diagnosis: string = '';
    time: string = "";
    cost: number = 0;
    doctor: UserSimpleData = new UserSimpleData;
    doctorId: number = 0;
    therapy: Therapy = new Therapy;
    patientId: number = 0;
}
export class UserSimpleData {
    id: number = 0;
    firstName: string = '';
    lastName: string = '';
    specialty: string = '';
}
export class Therapy {
    id: number = 0;
    startDate: string = '';
    endDate: string = '';
    perscription: string = '';
    appointmentId: number = 0
}
export class StringData {
    diagnosis: string = "";
}
