﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { PatientData } from './FetchPatient';
interface ViewPatientDataState {
    title: string;
    loading: boolean;
    empData: PatientData;
}
var divStyle = {
    height: 50,
    width: '55%',
    display: 'inline-block'
}
var colDivStyle = {
    margin: '10',
    width: '35%',
    display: 'inline-block'
}
var contStyle =
    {
        width: '90%',
        display: 'flex' 
    }
export class ViewPatient extends React.Component<RouteComponentProps<{}>, ViewPatientDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "", loading: true,  empData: new PatientData };

        

        var empid = this.props.match.params["empid"];
        // This will set state for Edit employee  
        if (empid > 0) {
            fetch('api/Patients/Details/' + empid)
                .then(response => response.json() as Promise<PatientData>)
                .then(data => {
                    this.setState({ title: "View", loading: false, empData: data});
                });
        }
        
      
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderViewPage();
        return <div>
            <h1>Patient Details</h1>
            <hr />
            {contents}
        </div>;
    }

    private handleView(id: number) {
        this.props.history.push("/patient/appointment/" + id);
    }
  
    // Returns the HTML Form to the render() method.  
    private renderViewPage() {
        return (
            <div className="row" style={contStyle} >
                <div className="col-sm" style={colDivStyle} >
                <div className="form-group row" >
                    <input type="hidden" name="employeeId" value={this.state.empData.id} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="First Name">First Name</label>
                    <div className="col-md-4">
                        <p  name="firstname">{this.state.empData.firstName}</p>
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Last Name">Last Name</label>
                    <div className="col-md-4">
                        <p  name="lastname" >{this.state.empData.lastName}</p>
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Date of birth">Date Of Birth</label>
                    <div className="col-md-4">
                        <p  name="dateofbirth" >{this.state.empData.dateOfBirth}</p>
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Jmbg">Jmbg</label>
                    <div className="col-md-4">
                        <p  name="jmbg">{this.state.empData.jmbg}</p>
                    </div>
                </div >
            </div >
                <div className="col-sm" style={divStyle}>
                    <h2>Appointments</h2>
                    <table className='table'>
                    <thead>
                        <tr>
                            
                            <th>Time</th>
                            <th>Doctor</th>
                            <th>Field</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.empData.appointments.map(emp =>
                            <tr key={emp.id}>
                               
                                    <td>{emp.time}</td>
                                    <td>{emp.doctor.firstName} {emp.doctor.lastName}</td>
                                    <td>{emp.doctor.specialty}</td>
                                <td>
                                        <a className="action" onClick={(id) => this.handleView(emp.id)}>View</a>
                                </td>
                            </tr>
                        )}
                    </tbody>
                    </table>

                    
                </div>
            </div>
          
        )
    }
}