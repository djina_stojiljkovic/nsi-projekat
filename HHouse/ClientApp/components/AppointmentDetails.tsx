﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { AppointmentData, PatientData } from './FetchPatient';
import { UserSimpleData } from './FetchPatient';
import { Therapy } from './FetchPatient';

interface AppointmentDetailsDataState {
    title: string;
    loading: boolean;
    empData: AppointmentData;
    patData: PatientData;
}
var divStyle = {
    height: 50,
    width: '55%',
    display: 'inline-block'
}
var colDivStyle = {
    margin: '10',
    width: '25%',
    display: 'inline-block'
}
var colDivStyle1 = {
    margin: '10',
    padding: '15',
    width: '20%',
    display: 'inline-block'
}
var contStyle =
    {
        width: '100%',
        display: 'flex'
    }

export class AppointmentDetails extends React.Component<RouteComponentProps<{}>, AppointmentDetailsDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "", loading: true, empData: new AppointmentData, patData: new PatientData };


        var empid = this.props.match.params["empid"];
        // This will set state for Edit employee  
        if (empid > 0) {
            fetch('api/Appointments/Details/' + empid)
                .then(response => response.json() as Promise<AppointmentData>)
                .then(data => {

                    fetch('api/Patients/Details/' + data.patientId)
                        .then(response => response.json() as Promise<PatientData>)
                        .then(data1 => {
                            this.setState({ title: "View", loading: false, empData: data, patData: data1 });
                        });

                  
                });
        }
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderViewPage();

        let contents2 = this.state.empData.therapy ? this.renderTherapy() : this.noTherapy();
        let contents3 = this.state.empData.diagnosis ? this.renderDiagnosis() : this.noDiagnosis();
        return <div>
            <h1>Appointment Details</h1>
            <hr />
            <div className="row" style={contStyle} >
                {contents}

                {contents3}
               {contents2}
                
            </div>
        </div>;
    }

    private handleView(id: number) {
        this.props.history.push("/addtherapy/" + id);
    }
    private handleView1(id: number) {
        this.props.history.push("/adddiagnosis/" + id);
    }
   

    


    // Returns the HTML Form to the render() method.  
    private renderViewPage() {
        return (
                <div className="col-sm" style={colDivStyle} >
                    <div className="form-group row" >
                        <input type="hidden" name="employeeId" value={this.state.empData.id} />
                    </div>
                    < div className="form-group row" >
                        <label className=" control-label col-md-12" htmlFor="Appointment cost">Appointment cost</label>
                        <div className="col-md-4">
                            <p name="cost">{this.state.empData.cost}</p>
                        </div>
                    </div >
                    < div className="form-group row" >
                        <label className=" control-label col-md-12" htmlFor="Doctor">Doctor</label>
                        <div className="col-md-4">
                            <p name="doctorName" >{this.state.empData.doctor.firstName} {this.state.empData.doctor.lastName}
                            </p>
                        </div>
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Patient">Patient</label>
                    <div className="col-md-4">
                        <p name="patientName" >{this.state.patData.firstName} {this.state.patData.lastName}
                        </p>
                    </div>
                </div>

                    < div className="form-group row" >
                        <label className=" control-label col-md-12" htmlFor="Appointment time">Appointment time</label>
                        <div className="col-md-4">
                        <p name="appointmentTime" >{this.state.empData.time} </p>
                        </div>
                    </div>
                

                    
                </div>
          

        )
    }

    private noTherapy()
    {
        return (

            <div className="col-sm" style={divStyle}>
                <h2>Therapy</h2>
                <a className="action" onClick={(id) => this.handleView(this.state.empData.id)}>Add Therapy</a> 
            </div>
            )
    }

    private noDiagnosis() {
        return (
            <div className="col-sm" style={colDivStyle1} >
            < div className="form-group row" >
                <label className=" control-label col-md-12" htmlFor="Diagnosis">Diagnosis</label>
                <div className="col-md-4">
                    <a className="action" onClick={(id) => this.handleView1(this.state.empData.id)}>Add Diagnosis</a>
                </div>
            </div > </div>
        )
    }

    private renderDiagnosis() {
        return (
            <div className="col-sm" style={colDivStyle1} >
            < div className="form-group row" >
                <label className=" control-label col-md-12" htmlFor="Diagnosis">Diagnosis</label>
                <div className="col-md-4">
                    {this.state.empData.diagnosis}
                </div>
            </div ></div>
        )
    }

    private renderTherapy()
    {
        return (
            
            <div className="col-sm" style={divStyle}>
                <h2>Therapy</h2>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Start date</th>
                        <th>End date</th>
                        <th>Perscription</th>
                    </tr>
                </thead>
                <tbody>
                    <tr key={this.state.empData.therapy.id}>
                        <td>{this.state.empData.therapy.startDate}</td>
                        <td>{this.state.empData.therapy.endDate}</td>
                        <td>{this.state.empData.therapy.perscription}</td>
                        <td> </td>
                    </tr>
                </tbody>
            </table> 

            </div>)
    }
}