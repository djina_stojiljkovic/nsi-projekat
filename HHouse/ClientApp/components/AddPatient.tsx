﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';  
import { RouteComponentProps } from 'react-router';  
import { Link, NavLink } from 'react-router-dom';  
import { PatientData } from './FetchPatient';  
interface AddPatientDataState {  
    title: string;  
    loading: boolean;  
    
    empData: PatientData;  
}  
export class AddPatient extends React.Component<RouteComponentProps<{}>, AddPatientDataState> {  
    constructor(props) {  
        super(props);
        this.state = { title: "", loading: true, empData: new PatientData };  
       
        var empid = this.props.match.params["empid"];  
        // This will set state for Edit employee  
        if (empid > 0) {  
            fetch('api/Patients/Details/' + empid)
                .then(response => response.json() as Promise<PatientData>)  
                .then(data => {  
                    this.setState({ title: "Edit", loading: false, empData: data });  
                });  
        }  
        // This will set state for Add employee  
        else {
            this.state = { title: "Create", loading: false, empData: new PatientData };  
        }  
        // This binding is necessary to make "this" work in the callback  
        this.handleSave = this.handleSave.bind(this);  
        this.handleCancel = this.handleCancel.bind(this);  
    }  
    public render() {  
        let contents = this.state.loading  
            ? <p><em>Loading...</em></p>  
            : this.renderCreateForm();  
        return <div>  
            <h1>{this.state.title}</h1>  
            <h3>Edit Patient</h3>  
            <hr />  
            {contents}  
        </div>;  
    }  
    // This will handle the submit form event.  
    private handleSave(event) {  
        event.preventDefault();  
        const data = new FormData(event.target);  
        // PUT request for Edit employee.  
        if (this.state.empData.id) {  
            fetch('api/Patients/Edit/'+ this.state.empData.id, {  
                method: 'PUT',  
                body: data,  
            }).then((response) => response.json())  
                .then((responseJson) => {  
                    this.props.history.push("/fetchpatient");  
                })  
        }  
        // POST request for Add employee.  
        else {  
            fetch('api/Patients', {  
                method: 'POST',  
                body: data,  
            }).then((response) => response.json())  
                .then((responseJson) => {  
                    this.props.history.push("/fetchpatient");  
                })  
        }  
    }  
    // This will handle Cancel button click event.  
    private handleCancel(e) {  
        e.preventDefault();  
        this.props.history.push("/fetchpatient");  
    }  
    // Returns the HTML Form to the render() method.  
    private renderCreateForm() {  
        return (  
            <form onSubmit={this.handleSave} >  
                <div className="form-group row" >
                    <input type="hidden" name="employeeId" value={this.state.empData.id} />  
                </div>  
                < div className="form-group row" >  
                    <label className=" control-label col-md-12" htmlFor="First Name">First Name</label>  
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="firstname" defaultValue={this.state.empData.firstName} required />  
                    </div>  
                </div >  
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Last Name">Last Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="lastname" defaultValue={this.state.empData.lastName} required />
                    </div>
                </div >  
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Date of birth">Date Of Birth</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="dateofbirth" defaultValue={this.state.empData.dateOfBirth} required />
                    </div>
                </div >  
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Jmbg">Jmbg</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="jmbg" defaultValue={this.state.empData.jmbg} required />
                    </div>
                </div >  

               
                
                <div className="form-group">  
                    <button type="submit" className="btn btn-default">Save</button>  
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>  
                </div >  
            </form >  
        )  
    }  
}