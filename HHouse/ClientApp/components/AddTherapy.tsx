﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX


import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { PatientData, Therapy } from './FetchPatient';

interface AddTherapyDataState {
    title: string;
    loading: boolean;

    empData: Therapy;
}
export class AddTherapy extends React.Component<RouteComponentProps<{}>, AddTherapyDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "", loading: true, empData: new Therapy };

        var empid = this.props.match.params['empid'];
        // This will set state for Edit employee  
        
        // This will set state for Add employee  
       
        this.state = { title: "Create", loading: false, empData: new Therapy };
       
        // This binding is necessary to make "this" work in the callback  
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm();
        return <div>
            <h1>{this.state.title}</h1>
            <h3>Edit Therapy</h3>
            <hr />
            {contents}
        </div>;
    }
    // This will handle the submit form event.  
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        // PUT request for Edit employee.  
        if (this.state.empData.id) {
            fetch('api/Patients/Edit/' + this.state.empData.id, {
                method: 'PUT',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchpatient");
                })
        }
        // POST request for Add employee.  
        else {
            fetch('api/Appointments/AddTherapy', {
                method: 'POST',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/appointment/details/" + this.props.match.params['empid']);
                })
        }
    }
    // This will handle Cancel button click event.  
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/fetchpatient");
    }
    // Returns the HTML Form to the render() method.  
    private renderCreateForm() {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="appointmentId" value={this.props.match.params["empid"]} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="StartDate">Start Date</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="startDate" defaultValue={this.state.empData.startDate} required />
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="EndDate">End Date</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="endDate" defaultValue={this.state.empData.endDate} required />
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Perscription">Perscription</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="perscription" defaultValue={this.state.empData.perscription} required />
                    </div>

                </div>

                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}