import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchPatient } from './components/FetchPatient';
import { FetchDoctors } from './components/FetchDoctors';
import { AddPatient } from './components/AddPatient';
import { ViewPatient } from './components/ViewPatient';
import { AppointmentDetails } from './components/AppointmentDetails';
import { FetchAppointments } from './components/FetchAppointments';
import { AddAppointment } from './components/AddAppointment';
import { AddTherapy } from './components/AddTherapy';
import { AddDiagnosis } from './components/AddDiagnosis';
import { AddDoctor } from './components/AddDoctor';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/fetchdoctors' component={FetchDoctors} />  
    <Route path='/fetchpatient' component={FetchPatient} />  
    <Route path='/addpatient' component={AddPatient} />
    <Route path='/patient/edit/:empid' component= {AddPatient} />
    <Route path='/patient/details/:empid' component= {ViewPatient} />  
    <Route path='/patient/appointment/:empid' component={AppointmentDetails} />  
    <Route path='/appointment/details/:empid' component={AppointmentDetails} />  
    <Route path='/fetchappointments' component={FetchAppointments} />  
    <Route path='/addappointment' component={AddAppointment} />  
    <Route path='/addtherapy/:empid' component={AddTherapy} /> 
    <Route path='/adddiagnosis/:empid' component={AddDiagnosis} /> 
    <Route path='/adddoctor' component={AddDoctor} />
    <Route path='/doctor/edit/:empid' component={AddDoctor} />

</Layout>;
