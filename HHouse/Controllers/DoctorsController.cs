﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HHouse.Models;
using HHouse.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace HHouse.Controllers
{
    public class DoctorsController : Controller
    {
        UserRepository repo = new UserRepository();

        [HttpGet]
        [Route("api/Doctors")]
        public IEnumerable<User> GetDoctors()
        {
            return repo.GetDoctors();
        }

        [HttpDelete]
        [Route("api/Doctors/{id}")]
        public int DeletePatient(long id)
        {
            return repo.DeleteDoctor(id);
        }

        [HttpPost]
        [Route("api/Doctors")]
        public int Create(User doctor)
        {
            return repo.AddDoctor(doctor);
        }

        [HttpGet]
        [Route("api/Doctors/Details/{id}")]
        public User GetDoctor(long id)
        {
            return repo.GetDoctor(id);
        }

        [HttpPut]
        [Route("api/Doctors/Edit/{id}")]
        public int UpdateDoctor([FromRoute] long id, User doctor)
        {
            doctor.Id = id;
            return repo.UpdateDoctor(doctor);
        }
    }

}