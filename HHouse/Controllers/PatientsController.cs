﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HHouse.Repositories;
using HHouse.Models;
using HHouse.Models.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HHouse.Controllers
{
    //[Route("api/[controller]")]
    public class PatientsController : Controller
    {
        PatientRepository repo = new PatientRepository();

        [HttpGet]
        [Route("api/Patients")]
        public IEnumerable<Patient> GetPatients()
        {
            return repo.GetAllPatients();
        }
        [HttpPost]
        [Route("api/Patients")]
        public int Create(Patient patient)
        {
            return repo.AddPatient(patient);
        }
        [HttpGet]
        [Route("api/Patients/Details/{id}")]
        public PatientViewModel getPatient(long id)
        {
            return repo.GetPatient(id);
        }

        [HttpGet]
        [Route("api/Patients/Name/{id}")]
        public string getPatientName(long id)
        {
            return repo.GetPatientName(id);
        }

        [HttpPut]
        [Route("api/Patients/Edit/{id}")]
        public int UpdatePatient([FromRoute] long id, Patient patient)
        {
            patient.Id = id;
            return repo.UpdatePatient(patient);
        }
        [HttpDelete]
        [Route("api/Patients/{id}")]
        public int DeletePatient(long id)
        {
            return repo.DeletePatient(id);
        }

        //[HttpGet]
        //[Route("api/Patients/{id}/appointments")]
        //public IEnumerable<Patient> GetPatientsAppointments([FromRoute] long id)
        //{
        //    return repo.GetPatientsAppointments(id);
        //}
    }
}
