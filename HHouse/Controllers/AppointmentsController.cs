﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HHouse.Repositories;
using HHouse.Models;
using HHouse.Models.ViewModels;

namespace HHouse.Controllers
{
    public class AppointmentsController : Controller
    {
        AppointmentRepositorycs repo = new AppointmentRepositorycs();

        [HttpGet]
        [Route("api/Appointments")]
        public IEnumerable<AppointmentViewModel> GetAppointments()
        {
            return repo.GetUpcoming();
        }
        [HttpPost]
        [Route("api/Appointments")]
        public int Create(Appointment appointment)
        {
            return repo.AddAppointment(appointment);
        }

        [HttpPost]
        [Route("api/Appointments/AddTherapy")]
        public void AddTherapy(Therapy therapy)
        {
             repo.AddTherapy(therapy);
        }
        [HttpPost]
        [Route("api/Appointments/AddDiagnosis/{id}")]
        public int AddDiagnosis([FromRoute] long id, DiagnosisViewModel d)
        {
            repo.AddDiagnosis(id, d.Diagnosis);
            return 1;
        }

        [HttpGet]
        [Route("api/Appointments/Details/{id}")]
        public AppointmentViewModel getAppointment(long id)
        {
            return repo.GetAppointment(id);
        }
        [HttpPut]
        [Route("api/Appointments/Edit/{id}")]
        public int UpdateAppointment([FromRoute] long id, Appointment a)
        {
            a.Id = id;
            return repo.UpdateAppointment(a);
        }
        [HttpDelete]
        [Route("api/Appointments/{id}")]
        public int DeleteAppointment(long id)
        {
            return repo.DeleteAppointment(id);
        }
    }
}