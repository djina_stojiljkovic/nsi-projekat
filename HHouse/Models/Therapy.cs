﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class Therapy
    {
        public long Id { get; set; }
        public long? AppointmentId { get; set; }
        public DateTime EndDate { get; set; }
        public string Perscription { get; set; }
        public DateTime StartDate { get; set; }
    }
}
