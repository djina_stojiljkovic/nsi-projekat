﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHouse.Models.ViewModels
{
    public class AppointmentViewModel
    {
        public long Id { get; set; }
        public string Cost { get; set; }
        public string Diagnosis { get; set; }
        public long DoctorId { get; set; }
        public bool? Finished { get; set; }
        public long? PatientId { get; set; }
        public String Time { get; set; }
        public TherapyViewModel Therapy { get; set; }
        public User Doctor { get; set; }
        public Patient Patient { get; set; }

        public AppointmentViewModel(Appointment a)
        {
            this.Cost = a.Cost;
            this.Id = a.Id;
            Diagnosis = a.Diagnosis;
            DoctorId = a.DoctorId;
            Finished = a.Finished;
            PatientId = a.PatientId;
            Time = a.Time.ToShortDateString() + " " + a.Time.ToShortTimeString();
            if(a.Therapy != null)
            {
                Therapy = new TherapyViewModel(a.Therapy);
            }
            else
            {
                Therapy = null;
            }
            Doctor = a.Doctor;
            Patient = null;
        }
    }
}
