﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHouse.Models.ViewModels
{
    public class DiagnosisViewModel
    {
        public string Diagnosis { get; set; }
    }
}
