﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHouse.Models.ViewModels
{
    public class TherapyViewModel
    {
        public long Id { get; set; }
        public long? AppointmentId { get; set; }
        public String EndDate { get; set; }
        public string Perscription { get; set; }
        public String StartDate { get; set; }

        public TherapyViewModel(Therapy t)
        {
            this.Id = t.Id;
            this.AppointmentId = t.AppointmentId;
            this.EndDate = t.EndDate.ToShortDateString() + " " + t.EndDate.ToShortTimeString();
            this.StartDate = t.StartDate.ToShortDateString() + " " + t.StartDate.ToShortTimeString();
            this.Perscription = t.Perscription;
        }
    }
}
