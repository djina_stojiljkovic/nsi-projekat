﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHouse.Models.ViewModels
{
    public class PatientViewModel
    {
        public long Id { get; set; }
        public string DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public long? GenderId { get; set; }
        public string Jmbg { get; set; }
        public string LastName { get; set; }
        public string Note { get; set; }
        public List<AppointmentViewModel> Appointments { get; set; }

        public PatientViewModel(Patient p)
        {
            this.Id = p.Id;
            this.DateOfBirth = p.DateOfBirth;
            FirstName = p.FirstName;
            GenderId = p.GenderId;
            Jmbg = p.Jmbg;
            LastName = p.LastName;
            Note = p.Note;
            this.Appointments = new List<AppointmentViewModel>();
            this.Appointments = p.Appointments.Select(a => new AppointmentViewModel(a)).ToList();
        }
    }
}
