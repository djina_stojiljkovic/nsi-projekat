﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class Role
    {
        public long Id { get; set; }
        public string RoleName { get; set; }
    }
}
