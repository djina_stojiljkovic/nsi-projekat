﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class Patient
    {
        public long Id { get; set; }
        public string DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public long? GenderId { get; set; }
        public string Jmbg { get; set; }
        public string LastName { get; set; }
        public string Note { get; set; }
        public List<Appointment> Appointments { get; set; }

        public Patient()
        {
            Appointments = new List<Appointment>();
        }
    }
}
