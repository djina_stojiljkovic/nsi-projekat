﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class Salary
    {
        public long Id { get; set; }
        public DateTime? DateOfPayment { get; set; }
        public long DoctorId { get; set; }
        public int? NumOfAppointments { get; set; }
        public double? Rate { get; set; }
        public DateTime? StartDate { get; set; }
        public double? Sum { get; set; }
    }
}
