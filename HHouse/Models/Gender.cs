﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class Gender
    {
        public long Id { get; set; }
        public string GenderName { get; set; }
    }
}
