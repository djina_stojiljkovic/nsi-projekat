﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class User
    {
        public bool? AtOffice { get; set; }
        public string Image { get; set; }
        public string Note { get; set; }
        public int? OrdinationNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Specialty { get; set; }
        public long Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long RoleId { get; set; }
        public int UserType { get; set; }
    }
}
