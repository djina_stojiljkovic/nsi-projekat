﻿using System;
using System.Collections.Generic;

namespace HHouse.Models
{
    public partial class Appointment
    {
        public long Id { get; set; }
        public string Cost { get; set; }
        public string Diagnosis { get; set; }
        public long DoctorId { get; set; }
        public bool? Finished { get; set; }
        public long? PatientId { get; set; }
        //public Patient Patient { get; set; }
        public DateTime Time { get; set; }
        public Therapy Therapy { get; set; }
        public User Doctor { get; set; }
    }
}
