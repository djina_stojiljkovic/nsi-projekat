﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HHouse.Models
{
    public partial class HHouseDbContext : DbContext
    {
        public HHouseDbContext()
        {
        }

        public HHouseDbContext(DbContextOptions<HHouseDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<Efmigrationshistory> Efmigrationshistory { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Salary> Salary { get; set; }
        public virtual DbSet<Therapy> Therapy { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("Server=localhost;database=HealthHouse;uid=root;persistsecurityinfo=True;password='';");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.ToTable("appointment");

                entity.HasIndex(e => e.DoctorId)
                    .HasName("IX_Appointment_DoctorId");

                entity.HasIndex(e => e.PatientId)
                    .HasName("IX_Appointment_PatientId");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.Diagnosis).HasMaxLength(2000);

                entity.Property(e => e.DoctorId).HasColumnType("bigint(20)");

                entity.Property(e => e.Finished).HasColumnType("bit(1)");

                entity.Property(e => e.PatientId).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Efmigrationshistory>(entity =>
            {
                entity.HasKey(e => e.MigrationId)
                    .HasName("PRIMARY");

                entity.ToTable("__efmigrationshistory");

                entity.Property(e => e.MigrationId).HasMaxLength(95);

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.ToTable("gender");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Patient>(entity =>
            {
                entity.ToTable("patient");

                entity.HasIndex(e => e.GenderId)
                    .HasName("IX_Patient_GenderId");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfBirth).HasMaxLength(30);

                entity.Property(e => e.FirstName).HasMaxLength(45);

                entity.Property(e => e.GenderId).HasColumnType("bigint(20)");

                entity.Property(e => e.Jmbg)
                    .HasColumnName("JMBG")
                    .HasMaxLength(13);

                entity.Property(e => e.LastName).HasMaxLength(45);

                entity.Property(e => e.Note).HasMaxLength(500);

                
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Salary>(entity =>
            {
                entity.ToTable("salary");

                entity.HasIndex(e => e.DoctorId)
                    .HasName("IX_Salary_DoctorId");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DoctorId).HasColumnType("bigint(20)");

                entity.Property(e => e.NumOfAppointments).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Therapy>(entity =>
            {
                entity.ToTable("therapy");

                entity.HasIndex(e => e.AppointmentId)
                    .HasName("IX_Therapy_AppointmentId")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.AppointmentId).HasColumnType("bigint(20)");

                entity.Property(e => e.Perscription).HasMaxLength(500);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_User_RoleId");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.AtOffice).HasColumnType("bit(1)");

                entity.Property(e => e.FirstName).HasMaxLength(45);

                entity.Property(e => e.Image).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(45);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.OrdinationNumber).HasColumnType("int(11)");

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.RoleId).HasColumnType("bigint(20)");

                entity.Property(e => e.Specialty).HasMaxLength(500);

                entity.Property(e => e.UserType).HasColumnType("int(11)");
            });
        }
    }
}
