﻿using HHouse.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HHouse.Repositories
{
    public class UserRepository
    {
        HHouseDbContext db = new HHouseDbContext();


        public IEnumerable<User> GetDoctors()
        {
            try
            {
                return db.User.Where(u => u.UserType == 2).ToList();
            }
            catch
            {
                throw;
            }
        }

        //To Delete the record of a particular employee    
        public int DeleteDoctor(long id)
        {
            try
            {
                var d = db.Set<User>()
                                      .Where(p => p.Id == id)
                                      .SingleOrDefault();

                ////Patient patient = db.Patient.Find(id);
                //List<Appointment> apps = patient.Appointments;
                //var list = apps.Select(a => db.Appointment.Remove(a)).ToList();
                db.User.Remove(d);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        public int AddDoctor(User doctor)
        {
            try
            {
                doctor.RoleId = 2;
                doctor.UserType = 2;
                db.User.Add(doctor);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        public User GetDoctor(long id)
        {
            try
            {
                //Patient patient = db.Patient.Find(id);
                var doctor = db.Set<User>()
                                      .Where(p => p.Id == id)
                                      .SingleOrDefault();
                return doctor;
            }
            catch (Exception e)
            {
                var m = e.Message;
                return null;
            }
        }

        public int UpdateDoctor(User doctor)
        {
            try
            {
                doctor.RoleId = 2;
                doctor.UserType = 2;
                db.Entry(doctor).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }
    }
}