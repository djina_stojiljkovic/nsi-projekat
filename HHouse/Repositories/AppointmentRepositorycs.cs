﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HHouse.Models;
using Microsoft.EntityFrameworkCore;
using HHouse.Models.ViewModels;

namespace HHouse.Repositories
{
    public class AppointmentRepositorycs
    {
        HHouseDbContext db = new HHouseDbContext();


        public IEnumerable<Appointment> GetAllAppointments()
        {
            try
            {
                var appointments = db.Set<Appointment>()
                                    .Include(a => a.Doctor)
                                     .ToList();
              
                return appointments;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AppointmentViewModel> GetUpcoming()
        {
            try
            {
                var appointments = db.Set<Appointment>()
                                        .Where(ap => ap.Time.CompareTo (DateTime.Now) > 0 )
                                        .Include(a => a.Doctor)
                                        .ToList();

              return appointments.Select(a => new AppointmentViewModel(a)).ToList();
            }
            catch
            {
                throw;
            }
        }

        //To Add new employee record     
        public int AddAppointment(Appointment app)
        {
            try
            {
                db.Appointment.Add(app);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        public void AddTherapy(Therapy therapy)
        {
            db.Set<Therapy>().Add(therapy);
            db.SaveChanges();

        }

        public void AddDiagnosis(long id, String d)
        {
            Appointment a = db.Appointment.Find(id);
            a.Diagnosis = d;
            db.Entry(a).State = EntityState.Modified;
            db.SaveChanges();


        }

        //To Update the records of a particluar employee    
        public int UpdateAppointment(Appointment app)
        {
            try
            {
                db.Entry(app).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }
        //Get the details of a particular employee    
        public AppointmentViewModel GetAppointment(long id)
        {
            try
            {
                var appointment = db.Set<Appointment>()
                                      .Where(a => a.Id == id)
                                      .Include(a => a.Doctor)
                                      .Include(a => a.Therapy)
                                      .SingleOrDefault();

                AppointmentViewModel vm = new AppointmentViewModel(appointment);
                return vm;

                //Appointment app = db.Appointment.Find(id);
                
                //return app;
            }
            catch
            {
                throw;
            }
        }
        //To Delete the record of a particular employee    
        public int DeleteAppointment(long id)
        {
            try
            {
                Appointment a = db.Appointment.Find(id);
                db.Appointment.Remove(a);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateChanges(Appointment entity)
        {
            var entityEntry = db.Entry(entity);
            var currentValues = entityEntry.CurrentValues;
            currentValues.SetValues(entity);

            var propertyEntries = entityEntry.Properties.Where(item => item.OriginalValue != null && !item.OriginalValue.Equals(item.CurrentValue));
            foreach (var propertyEntry in propertyEntries)
            {
                propertyEntry.IsModified = true;
            }
        }
    }
}
