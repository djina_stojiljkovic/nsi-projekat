﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HHouse.Models;
using HHouse.Models.ViewModels;

namespace HHouse.Repositories
{
    public class PatientRepository
    {
        HHouseDbContext db = new HHouseDbContext();


        public IEnumerable<Patient> GetAllPatients()
        {
            try
            {
                return db.Patient.ToList();
            }
            catch
            {
                throw;
            }
        }
        //To Add new employee record     
        public int AddPatient(Patient patient)
        {
            try
            {
                db.Patient.Add(patient);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }
        //To Update the records of a particluar employee    
        public int UpdatePatient(Patient patient)
        {
            try
            {
                db.Entry(patient).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }
        //Get the details of a particular employee    
        public PatientViewModel GetPatient(long id)
        {
            try
            {
                //Patient patient = db.Patient.Find(id);
                var patient = db.Set<Patient>()
                                      .Where(p => p.Id == id)
                                      .Include(p => p.Appointments)
                                      .ThenInclude(a => a.Doctor)
                                      .SingleOrDefault();
                return new PatientViewModel(patient);
            }
            catch(Exception e)
            {
                var m = e.Message;
                return null;
            }
        }
        public string GetPatientName(long id)
        {
            try
            {
                //Patient patient = db.Patient.Find(id);
                var patient = db.Set<Patient>()
                                      .Where(p => p.Id == id)
                                      .SingleOrDefault();
                return patient.FirstName + " " + patient.LastName;
            }
            catch
            {
                throw;
            }
        }


        //To Delete the record of a particular employee    
        public int DeletePatient(long id)
        {
            try
            {
                var patient = db.Set<Patient>()
                                      .Where(p => p.Id == id)
                                      .Include(p => p.Appointments)
                                      .SingleOrDefault();

                //Patient patient = db.Patient.Find(id);
                List<Appointment> apps = patient.Appointments;
                var list = apps.Select(a => db.Appointment.Remove(a)).ToList();
                db.Patient.Remove(patient);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //public IEnumerable<Patient>  GetPatientsAppointments(long id)
        //{
        //    var patients = db.Set<Patient>()
        //                              .Where(p => p.Id == id)
        //                              .Include(p => p.Appointments)
        //                              .ToList();
        //    return patients;
        //}
        
    }
}

