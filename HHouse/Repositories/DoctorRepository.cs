﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HHouse.Models;
using Microsoft.EntityFrameworkCore;
using HHouse.Models.ViewModels;

namespace HHouse.Repositories
{
    public class DoctorRepository
    {
        HHouseDbContext db = new HHouseDbContext();


        public IEnumerable<User> GetAllDoctors()
        {
            try
            {
                var doctors = db.Set<User>().Where(d => d.UserType == 2)
                                    .ToList();

                return doctors;
            }
            catch
            {
                throw;
            }
        }


        public int AddDoctor(User doctor)
        {
            try
            {
                db.User.Add(doctor);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }



    }
}
